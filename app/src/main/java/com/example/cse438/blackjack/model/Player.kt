package com.example.cse438.blackjack.model

import com.example.cse438.blackjack.util.FirebaseUtil
import com.google.firebase.firestore.Exclude

class Player() {
    var name: String = ""
    var uid: String = ""
    var numOfWins: Int = 0
    var numOfLosses: Int = 0
    var money = 500

    constructor(name: String, uid: String, numOfWins: Int, numOfLosses: Int, money: Int = 500): this() {
        this.name = name
        this.uid = uid
        this.numOfWins = numOfWins
        this.numOfLosses = numOfLosses
        this.money = money
    }

    val winPercentage: Float
        get()  {
        val total = numOfWins + numOfLosses
        var winPercent = 0f
        if (total > 0) {
            winPercent = numOfWins.toFloat() / total.toFloat()
        }
        return winPercent
    }

}