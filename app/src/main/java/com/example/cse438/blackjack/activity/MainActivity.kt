package com.example.cse438.blackjack.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import com.example.cse438.blackjack.R
import com.example.cse438.blackjack.enum.UserInterfaceState
import com.example.cse438.blackjack.fragment.GameFragment
import com.example.cse438.blackjack.fragment.LeaderboardFragment
import com.example.cse438.blackjack.fragment.NoConnectionFragment
import com.example.cse438.blackjack.fragment.NotLoggedInFragment
import com.example.cse438.blackjack.util.FirebaseUtil
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    var currentView = UserInterfaceState.GAME
    private var isNetworkConnected = false
    private val RC_SIGN_IN = 123

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Setup the nav bar
        setSupportActionBar(toolbar)
        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)
        //nav_view.setCheckedItem(R.id.nav_home)

        // Setup internet connection
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo

        // Load Fragment into View
        val fm = supportFragmentManager

        // add
        val ft = fm.beginTransaction()

        if (networkInfo != null) {
            Log.d("NETWORK", "connected")
            this.isNetworkConnected = true
        }

        if (!this.isNetworkConnected) {
            Log.e("NETWORK", "not connected")
            ft.add(R.id.frag_placeholder, NoConnectionFragment(), "NO_CONNECTION_FRAG")
            currentView = UserInterfaceState.NETWORK_ERROR
        }
        else if (FirebaseUtil.CurrentUser == null) {
            // Bring user to sign in page
            createSignInIntent()
        }
        else if (FirebaseUtil.CurrentUser != null) {
            val name = FirebaseUtil.CurrentUser?.displayName
            Toast.makeText(this, "Welcome back $name", Toast.LENGTH_SHORT).show()
            nav_view.menu.findItem(R.id.nav_sign_in_out).title = getString(R.string.sign_out)
            ft.add(R.id.frag_placeholder, GameFragment(this@MainActivity), "GAME_FRAG")
                .addToBackStack(null)
            nav_view.setCheckedItem(R.id.nav_game)
            currentView = UserInterfaceState.GAME
        }

        // Commit the fragment
        ft.commit()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
            if (currentView == UserInterfaceState.GAME) {
                nav_view.setCheckedItem(R.id.nav_leaderboard)
                supportActionBar?.title = "Leaderboard"
                supportActionBar?.subtitle = ""
            }
            else {
                nav_view.setCheckedItem(R.id.nav_game)
                supportActionBar?.title = "Blackjack"
                supportActionBar?.subtitle = ""
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_sign_in_out -> {
                if (this.isNetworkConnected) {
                    if (FirebaseUtil.CurrentUser == null) {
                        createSignInIntent()
                        item.title = "Sign Out"
                    } else {
                        signOut()
                        item.title = "Sign In"
                    }
                }
            }
            R.id.nav_game -> {
                if (FirebaseUtil.CurrentUser != null) {
                    replaceFragment(UserInterfaceState.GAME)
                }
                else {
                    replaceFragment(UserInterfaceState.NOT_SIGNED_IN)
                }
            }
            R.id.nav_leaderboard -> {
                replaceFragment(UserInterfaceState.LEADERBOARD)
            }
            R.id.nav_new_game -> {
                // Make sure the user is logged in
                if (FirebaseUtil.CurrentUser != null) {
                    // Load the game fragment if it isn't already loaded. This will automatically
                    // start a new game
                    if (currentView != UserInterfaceState.GAME) {
                        replaceFragment(UserInterfaceState.GAME)
                    }
                    else {
                        // Grab the game fragment and call the newGame function
                        var fragment = supportFragmentManager.findFragmentById(R.id.frag_placeholder) as GameFragment
                        fragment.newGame()
                    }
                }
                else {
                    replaceFragment(UserInterfaceState.NOT_SIGNED_IN)
                }
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun createSignInIntent() {
        // [START auth_fui_create_intent]
        // Choose authentication providers
        val providers = arrayListOf(
            AuthUI.IdpConfig.EmailBuilder().build(),
            AuthUI.IdpConfig.GoogleBuilder().build()) // TODO

        // Create and launch sign-in intent
        startActivityForResult(
            AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setLogo(R.drawable.back)
                .setAvailableProviders(providers)
                .build(),
            RC_SIGN_IN)
        // [END auth_fui_create_intent]
    }

    private fun signOut() {
        AuthUI.getInstance()
            .signOut(this)
            .addOnCompleteListener {
                Log.d("SIGN_OUT", "Successfully signed out")
                if (currentView == UserInterfaceState.GAME) {
                    // TODO - probably some game cleanup
                    replaceFragment(UserInterfaceState.NOT_SIGNED_IN)
                }
                Toast.makeText(this@MainActivity, "Successfully signed out", Toast.LENGTH_SHORT).show()
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {
                // Successfully signed in
                Log.d("SIGN_IN", "Successfully signed in")
                replaceFragment(UserInterfaceState.GAME)
                Toast.makeText(this@MainActivity, "Successfully signed in", Toast.LENGTH_SHORT).show()
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
                if (response == null) {
                    Log.d("SIGN_IN", "Player canceled the sign-in flow using the back button")
                }
                else {
                    Log.e("SIGN_IN", "Error signing in: ${response.error?.errorCode}")
                }
                nav_view.menu.findItem(R.id.nav_sign_in_out).title = getString(R.string.sign_in)
                replaceFragment(UserInterfaceState.NOT_SIGNED_IN)
            }
        }
    }

    private fun replaceFragment(state: UserInterfaceState) {
        if (currentView != state) {
            val fm = supportFragmentManager
            val ft = fm.beginTransaction()
            when (state) {
                UserInterfaceState.NETWORK_ERROR -> {
                    ft.replace(
                        R.id.frag_placeholder,
                        NoConnectionFragment(),
                        "NO_NETWORK_FRAG"
                    )
                    supportActionBar?.title = "No Network"
                    supportActionBar?.subtitle = ""
                }
                UserInterfaceState.NOT_SIGNED_IN -> {
                    ft.replace(
                        R.id.frag_placeholder,
                        NotLoggedInFragment(),
                        "NOT_SIGNED_IN_FRAG"
                    )
                    supportActionBar?.title = "Not Signed In"
                    supportActionBar?.subtitle = ""
                }
                UserInterfaceState.GAME -> {
                    ft.replace(
                        R.id.frag_placeholder,
                        GameFragment(this@MainActivity),
                        "GAME_FRAG"
                    ).addToBackStack(null)
                    supportActionBar?.title = "Blackjack"
                    supportActionBar?.subtitle = ""
                    nav_view.setCheckedItem(R.id.nav_game)
                }
                UserInterfaceState.LEADERBOARD -> {
                    ft.replace(
                        R.id.frag_placeholder,
                        LeaderboardFragment(this@MainActivity),
                        "LEADERBOARD_FRAG"
                    ).addToBackStack(null)
                    supportActionBar?.title = "Leaderboard"
                    supportActionBar?.subtitle = ""
                    nav_view.setCheckedItem(R.id.nav_leaderboard)
                }
            }
            currentView = state
            ft.commit()
        }
    }
}
