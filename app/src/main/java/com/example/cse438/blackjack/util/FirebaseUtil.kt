package com.example.cse438.blackjack.util

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class FirebaseUtil {
    companion object {
        private var auth: FirebaseAuth? = null

        val Auth: FirebaseAuth
            get() {
                if (auth == null) {
                    auth = FirebaseAuth.getInstance()
                }
                return auth!!
            }

        val CurrentUser
            get() = Auth?.currentUser

        val FirestoreDb
            get() = FirebaseFirestore.getInstance()
    }
}