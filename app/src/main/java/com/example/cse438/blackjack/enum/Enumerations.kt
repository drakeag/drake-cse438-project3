package com.example.cse438.blackjack.enum

enum class UserInterfaceState {
    NETWORK_ERROR,
    NOT_SIGNED_IN,
    GAME,
    LEADERBOARD
}

// From the player's perspective
enum class GameResult {
    WIN,
    LOSE,
    PUSH,
    BLACKJACK
}