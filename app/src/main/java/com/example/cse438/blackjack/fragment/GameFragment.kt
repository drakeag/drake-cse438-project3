package com.example.cse438.blackjack.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.view.GestureDetectorCompat
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast

import com.example.cse438.blackjack.R
import com.example.cse438.blackjack.enum.GameResult
import com.example.cse438.blackjack.util.GameManager
import com.example.cse438.blackjack.model.Player
import com.example.cse438.blackjack.util.CardRandomizer
import com.example.cse438.blackjack.util.FirebaseUtil
import com.google.firebase.firestore.DocumentReference
import kotlinx.android.synthetic.main.dialog_bet.*
import kotlinx.android.synthetic.main.fragment_game.*
import java.util.*
import kotlin.collections.HashMap

@SuppressLint("ValidFragment")
class GameFragment(context: Context) : Fragment() {
    private val TAG = "GAME_FRAG"
    private var parentContext = context
    private lateinit var mDetector: GestureDetectorCompat
    private var height: Int = 0
    private var width: Int = 0
    private val CARD_HEIGHT = 350
    private val CARD_WIDTH = 300
    private val imgViews = ArrayList<View>()
    private var numDealerCardsVisible = 0 // The number of dealer cards currently visible
    private var numPlayerCardsVisible = 0 // The number of player cards currently visible

    private var isInitialized: Boolean = false
    private lateinit var cardList: ArrayList<Int>
    private lateinit var cardMap: HashMap<Int, String>
    private lateinit var gameManager: GameManager
    private var gameFinished = false
    private lateinit var playerRef: DocumentReference
    private lateinit var currentPlayer: Player

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView()")
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_game, container, false)

        // Setup custom onTouchEvent callback for this fragment
        view.setOnTouchListener { v, event ->  onTouchEvent(event) }

        // Setup the gesture listener to respond to user touches
        mDetector = GestureDetectorCompat(parentContext, MyGestureListener())

        // Get the window size for moving cards around
        val metrics = this.resources.displayMetrics
        this.height = metrics.heightPixels
        this.width = metrics.widthPixels

        Log.d(TAG, "Height: $height")
        Log.d(TAG, "Width: $width")

        // Initialize everything that only needs to be initialized once
        if (!isInitialized) {
            init()
        }

        return view
    }

    override fun onStart() {
        Log.d(TAG, "onStart()")
        super.onStart()
    }

    override fun onResume() {
        Log.d(TAG, "onResume()")
        super.onResume()
        if (::currentPlayer.isInitialized == false) {
            // Try to create a Player object for the current user
            playerRef.get()
                .addOnSuccessListener { document ->
                    if (document != null && document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: ${document.data}")
                        currentPlayer = document.toObject(Player::class.java)!!
                    } else {
                        Log.d(TAG, "No such document - creating new player")
                        val name = FirebaseUtil.CurrentUser?.displayName!!
                        val uid = FirebaseUtil.CurrentUser?.uid!!
                        currentPlayer = Player(name, uid, 0, 0)
                        addPlayer(currentPlayer)
                    }

                    // Update the UI to reflect the current player's number of wins/losses
                    updateWinLossesUI()

                    // Start a new game
                    newGame()
                }
                .addOnFailureListener { exception ->
                    Log.d(TAG, "player get failed with ", exception)
                }
        }
        else {
            // Update the UI to reflect the current player's number of wins/losses
            updateWinLossesUI()

            // Start a new game
            newGame()
        }
    }

    private fun init() {
        if (!isInitialized) {
            // Get a player reference
            playerRef = FirebaseUtil.FirestoreDb.collection("players").document(FirebaseUtil.CurrentUser?.uid!!)

            // Create a list of cards
            val randomizer = CardRandomizer()
            cardList = randomizer.getIDs(parentContext)

            // Map the card IDs to their respective names
            cardMap = HashMap()
            for (cardId in cardList) {
                cardMap[cardId] = resources.getResourceEntryName(cardId)
            }

            // Initialize the GameManager
            gameManager = GameManager(cardList, cardMap)

            isInitialized = true
        }
    }

    //region Firebase functions
    // Adds a new player to the Firestore database
    private fun addPlayer(player: Player) {
        if (currentPlayer != null) {
            val db = FirebaseUtil.FirestoreDb
            db.collection("players").document(currentPlayer.uid).set(player)
                .addOnSuccessListener { Log.d(TAG, "Player successfully added!") }
                .addOnFailureListener { e -> Log.e(TAG, "Error adding player", e) }
        }
    }

    // Updates the player's stats in the Firestore database
    private fun updatePlayer(result: GameResult) {
        Log.d(TAG,"Updating player - $result")
        when(result) {
            GameResult.WIN -> {
                currentPlayer.numOfWins++
                currentPlayer.money += gameManager.playerBet
            }
            GameResult.LOSE -> {
                currentPlayer.numOfLosses++
                currentPlayer.money -= gameManager.playerBet
                // Reset the player's money to $50 if they run out
                if (currentPlayer.money <= 0) {
                    currentPlayer.money = 50
                }
            }
            GameResult.BLACKJACK -> {
                // Give the player 1.5x their bet for a blackjack
                currentPlayer.money += (1.5 * gameManager.playerBet).toInt()
            }
        }
        playerRef.update(
            "numOfWins", currentPlayer.numOfWins,
            "numOfLosses", currentPlayer.numOfLosses,
            "winPercentage", currentPlayer.winPercentage,
            "money", currentPlayer.money
        )
            .addOnSuccessListener {
                Log.d(TAG, "Player successfully updated!")
                updateWinLossesUI()
            }
            .addOnFailureListener { e -> Log.e(TAG, "Error updating player", e) }
    }
    //endregion

    //region Game functions
    // Starts a new game
    fun newGame() {
        Log.d(TAG, "New game started")
        // Clear any cards from a previous game off the screen
        clearImgViews()

        // Update the money and scores on the UI
        moneyTv.text = getString(R.string.player_money_label, currentPlayer.money)
        dealerScoreTv.text = getString(R.string.dealer_score_label, 0)
        playerScoreTv.text = getString(R.string.player_score_label, 0)

        // Initialize variables for new game
        gameFinished = false
        updateResultUI("")

        // Place bet...once this finishes it will call initNewGame to deal cards and begin
        showBetDialog()
    }

    private fun initNewGame(bet: Int) {
        // Initialize a new game
        gameManager.newGame(bet)

        // Move the player cards the right locations
        addPlayerCardToUI(gameManager.playerHand[0])
        addPlayerCardToUI(gameManager.playerHand[1])

        // Move the dealer cards to the right locations
        addDealerCardToUI(R.drawable.back)
        addDealerCardToUI(gameManager.dealerUpCardId)

        // Update the dealer and player scores on the UI
        updateScoreUI(false)

        Log.d(TAG, "Player cards: ${gameManager.getPlayerCardsString()}")
        Log.d(TAG, "Player hand at ${gameManager.PlayerHandValue}")
    }

    // Draws a card for the player
    private fun playerHit() {
        if (!gameFinished) {
            val cardId = gameManager.playerHit()
            Log.d(TAG, "Hit - player drew ${cardMap[cardId]}")
            Log.d(TAG, "Player cards: ${gameManager.getPlayerCardsString()}")

            // Move card
            addPlayerCardToUI(cardId)

            // Determine new hand value
            val handValue = gameManager.PlayerHandValue
            Log.d(TAG, "Player hand at $handValue")

            when {
                // Bust
                handValue > 21 -> {
                    Log.d(TAG, "Player bust - dealer wins")
                    // Game over - player loses

                    // Update the UI
                    updateScoreUI(true)
                    updateResultUI(getString(R.string.player_busted))

                    // Increment losses
                    updatePlayer(GameResult.LOSE)

                    startDelayedNewGame(5)
                }
                // Blackjack
                handValue == GameManager.BLACKJACK -> {
                    Log.d(TAG, "Player blackjack")
                    // Update the UI
                    updateScoreUI(false)

                    // Game over - check the dealer's hand
                    dealerTurn()
                }
                // The game can continue
                else -> {
                    // Update the scores on the UI
                    updateScoreUI(false)
                }
            }
        }
    }

    // Starts the dealer's turn
    private fun dealerTurn() {
        var handValue = 0
        var playerHandValue = 0
        var dealersTurn = true
        while (dealersTurn) {
            handValue = gameManager.DealerHandValue
            playerHandValue = gameManager.PlayerHandValue
            Log.d(TAG, "Dealer hand at $handValue")
            when {
                // Push
                handValue == playerHandValue -> {
                    Log.d(TAG, "Push - nobody wins")
                    showHoleCard()
                    dealersTurn = false

                    // Update the UI
                    updatePlayer(GameResult.PUSH)
                    updateScoreUI(true)
                    updateResultUI(getString(R.string.push))
                }
                // Blackjack
                handValue == GameManager.BLACKJACK -> {
                    Log.d(TAG, "Blackjack - dealer wins")
                    showHoleCard()
                    dealersTurn = false

                    // Increment losses
                    updatePlayer(GameResult.LOSE)

                    // Update the UI
                    updateScoreUI(true)
                    updateResultUI(getString(R.string.dealer_blackjack))
                }
                // Bust
                handValue > 21 -> {
                    Log.d(TAG, "Dealer bust - player wins")
                    showHoleCard()
                    dealersTurn = false

                    // Increment wins after checking if player blackjack
                    if (playerHandValue == GameManager.BLACKJACK) {
                        updatePlayer(GameResult.BLACKJACK)
                    }
                    else {
                        updatePlayer(GameResult.WIN)
                    }

                    // Update UI
                    updateScoreUI(true)
                    updateResultUI(getString(R.string.dealer_busted))
                }
                // Stay if hand is greater than or equal to 17
                handValue >= 17 -> {
                    Log.d(TAG, "Dealer stays")
                    showHoleCard()
                    if (handValue > gameManager.PlayerHandValue) {
                        Log.d(TAG, "Dealer wins")
                        // Increment losses
                        updatePlayer(GameResult.LOSE)

                        // Update the UI
                        updateScoreUI(true)
                        updateResultUI(getString(R.string.player_loses))
                    }
                    else {
                        Log.d(TAG, "Player wins")
                        // Increment wins after checking if player blackjack
                        if (playerHandValue == GameManager.BLACKJACK) {
                            updatePlayer(GameResult.BLACKJACK)
                        }
                        else {
                            updatePlayer(GameResult.WIN)
                        }

                        // Update the UI
                        updateScoreUI(true)
                        updateResultUI(getString(R.string.player_wins))
                    }
                    dealersTurn = false
                }
                // Hit if hand is less than 17
                handValue < 17 -> {
                    val cardId = gameManager.dealerHit()
                    addDealerCardToUI(cardId)
                    Log.d(TAG, "Hit - dealer drew ${cardMap[cardId]}")
                }
            }
        }

        startDelayedNewGame(5)
    }

    // Starts a new game in the specified amount of time
    private fun startDelayedNewGame(secDelay: Long) {
        Toast.makeText(parentContext, "Starting new game in $secDelay seconds", Toast.LENGTH_LONG).show()
        Handler().postDelayed({
           newGame()
        }, secDelay*1000)

    }
    //endregion

    //region UI functions
    // Show the dialog to enter in a bet before the cards are dealt
    private fun showBetDialog() {
        val view = layoutInflater.inflate(R.layout.dialog_bet, null)
        val dialog: AlertDialog = AlertDialog.Builder(context)
            .setTitle("Make Bet")
            .setCancelable(false)
            .setView(view)
            .setPositiveButton("Bet", null)
            .setNegativeButton(android.R.string.cancel) { dialog, p1 ->
                dialog.cancel()
            }
            .create()

        // Get the form data
        val betView = view.findViewById<EditText>(R.id.betTv)

        dialog.show()

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(object: View.OnClickListener {
            override fun onClick(view: View) {
                if (betView.text.isNullOrBlank()) {
                    betView.error = "Bet cannot be empty!"
                }
                else {
                    val bet = betView.text.toString().toInt()
                    if (bet < GameManager.MIN_BET || bet > GameManager.MAX_BET) {
                        betView.error = getString(R.string.invalid_bet_amount_error,
                                                    GameManager.MIN_BET,
                                                    GameManager.MAX_BET)
                    } else {
                        dialog.dismiss()
                        initNewGame(bet)
                    }
                }
            }
        })
    }

    // Displays a message on the center on the screen when a game is over
    private fun updateResultUI(message: String) {
        moneyTv.text = getString(R.string.player_money_label, currentPlayer.money)
        gameResultTv.text = message
    }

    // Updates the win and losses on the UI
    private fun updateWinLossesUI() {
        winsTv.text = getString(R.string.num_wins_label, currentPlayer.numOfWins)
        lossTv.text = getString(R.string.num_losses_label, currentPlayer.numOfLosses)
    }

    // Updates the dealer and player hand values on the UI
    private fun updateScoreUI(gameOver: Boolean) {
        if (gameOver) {
            dealerScoreTv.text = getString(R.string.dealer_score_label, gameManager.DealerHandValue)
            gameFinished = true
        } else {
            dealerScoreTv.text = getString(R.string.dealer_score_label, gameManager.DealerShownHandValue)
        }
        playerScoreTv.text = getString(R.string.player_score_label, gameManager.PlayerHandValue)
    }

    // Creates an ImageView for a card and add it to the layout as invisible
    private fun createCardImgView(cardId: Int, zIndex: Float): View {

        // Create a new ImageView object
        val cardImgView = ImageView(parentContext)

        // Add it to the imgViews array so we can keep track of all created ImageViews
        imgViews.add(cardImgView)

        // Add it to the game fragment layout
        gameLayout.addView(cardImgView)

        // Set the height and width
        cardImgView.layoutParams.height = CARD_HEIGHT
        cardImgView.layoutParams.width = CARD_WIDTH

        // Set the image to the resource ID provided
        cardImgView.setImageResource(cardId)
        //cardImgView.setImageResource(R.drawable.back)

        // Set initial position to the center of the screen
        cardImgView.translationX = width/2f - CARD_WIDTH/2f
        cardImgView.translationY = height/2f - CARD_HEIGHT/2f
        cardImgView.translationZ = zIndex

        // Set it to be invisible at first
        cardImgView.visibility = View.INVISIBLE

        return cardImgView
    }

    // Clears any card ImageViews in the layout
    private fun clearImgViews() {
        for (view in imgViews) {
            gameLayout.removeView(view)
        }
        imgViews.clear()
        numDealerCardsVisible = 0
        numPlayerCardsVisible = 0
    }

    // Adds and moves a player's card to the appropriate location in the layout
    private fun addPlayerCardToUI(cardId: Int) {
        val targetX = CARD_WIDTH/4f + numPlayerCardsVisible * 100 // Shift the card to the right by n*100
        val targetY = height.toFloat() - CARD_HEIGHT*2 - playerScoreTv.height
        val card = createCardImgView(cardId, numPlayerCardsVisible.toFloat())
        moveCard(card, targetX, targetY)
        numPlayerCardsVisible++
    }

    // Adds and moves a dealer's card to the appropriate locations in the layout
    private fun addDealerCardToUI(cardId: Int) {
        val targetX = CARD_WIDTH/4f + numDealerCardsVisible * 100 // Shift the card to the right by n*100
        val targetY = 0f + CARD_HEIGHT/2 + dealerScoreTv.height
        val card = createCardImgView(cardId, numDealerCardsVisible.toFloat())
        moveCard(card, targetX, targetY)
        numDealerCardsVisible++
    }

    // Shows the flipped down dealer card on the UI
    private fun showHoleCard() {
        val targetX = CARD_WIDTH/4f + 0 * 100 // Shift the card to the right by n*100
        val targetY = 0f + CARD_HEIGHT/2 + dealerScoreTv.height
        val card = createCardImgView(gameManager.holeCardId, 0f)
        moveCard(card, targetX, targetY)
    }

    // Moves a card ImageView to (targetX, targetY)
    private fun moveCard(card: View, targetX: Float, targetY: Float, duration: Long = 600) {
        Log.d(TAG, "Moving card to ($targetX, $targetY)")
        card.visibility = View.VISIBLE
        card.animate().translationX(targetX).translationY(targetY).duration = duration
    }
    //endregion

    private fun onTouchEvent(event: MotionEvent) : Boolean {
        mDetector.onTouchEvent(event)
        return true
    }

    private inner class MyGestureListener : GestureDetector.SimpleOnGestureListener() {
        private var swipedistance = 150

        override fun onLongPress(event: MotionEvent) {
            Log.d("GESTURE_LISTENER", "long press - no action")
            //moveTo(this@MainActivity.width / 2f - face.width / 2, -this@MainActivity.height / 2f + face.height / 2f)
        }

        override fun onDoubleTap(e: MotionEvent?): Boolean {
            //moveTo(-this@MainActivity.width / 2f + face.width / 2, this@MainActivity.height / 2f - face.height / 2f)
            Log.d("GESTURE_LISTENER", "double tap - stand")
            dealerTurn()
            return true
        }

        override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
            //moveTo(0f, 0f)
            Log.d("GESTURE_LISTENER", "single tap - no action")
            return true
        }

        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            if(e1.x - e2.x > swipedistance) {
                Log.d("GESTURE_LISTENER", "left swipe - no action")
                //moveTo(-this@MainActivity.width / 2f + face.width / 2, 0f)
                return true
            } else if (e2.x - e1.x > swipedistance) {
                Log.d("GESTURE_LISTENER", "right swipe - hit")
                //moveTo(this@MainActivity.width / 2f - face.width / 2, 0f)
                playerHit()
                return true
            } else if(e1.y - e2.y > swipedistance) {
                Log.d("GESTURE_LISTENER", "up swipe - no action")
                //moveTo(0f, -this@MainActivity.height / 2f + face.height / 2f)
                return true
            } else if (e2.y - e1.y > swipedistance) {
                Log.d("GESTURE_LISTENER", "down swipe - no action")
                //moveTo(0f, this@MainActivity.height / 2f - face.height / 2f)
                return true
            }
            return false
        }
    }
}
