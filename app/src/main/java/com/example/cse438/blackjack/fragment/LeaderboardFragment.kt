package com.example.cse438.blackjack.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.example.cse438.blackjack.R
import com.example.cse438.blackjack.model.Player
import com.example.cse438.blackjack.util.FirebaseUtil
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.fragment_leaderboard.*
import kotlinx.android.synthetic.main.leaderboard_list_item.view.*

@SuppressLint("ValidFragment")
class LeaderboardFragment(context: Context) : Fragment() {
    private val TAG = "LEADERBOARD_FRAG"
    private var parentContext: Context = context
    private lateinit var playersRef: DocumentReference
    private val players = ArrayList<Player>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_leaderboard, container, false)
    }

    override fun onStart() {
        super.onStart()

        // Get the top 50 players from Firebase database
        val playersQuery = FirebaseUtil.FirestoreDb
            .collection("players")
            .orderBy("winPercentage", Query.Direction.DESCENDING)
            .limit(50)
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    //Log.d(TAG, "${document.id} => ${document.data}")
                    players.add(document.toObject(Player::class.java)!!)
                }

                //Log.d(TAG, "Number of players returned: {$players.size}")

                // Setup RecycleView
                leaderboard_list.layoutManager = LinearLayoutManager(parentContext)
                leaderboard_list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
                leaderboard_list.adapter = LeaderboardAdapter(players)
            }
            .addOnFailureListener { exception ->
                Log.d(TAG, "Error getting players: ", exception)
            }
    }

    inner class LeaderboardAdapter(private val sortedData: ArrayList<Player>): RecyclerView.Adapter<LeaderboardAdapter.LeaderboardViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeaderboardViewHolder {
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.leaderboard_list_item, parent, false)
            return LeaderboardViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: LeaderboardViewHolder, position: Int) {
            val user = sortedData[position]
            holder.rank.text = "${position+1}"
            holder.userName.text = user.name
            holder.money.text = "$" + user.money.toString()
            holder.wins.text = user.numOfWins.toString()
            holder.losses.text = user.numOfLosses.toString()
            holder.winPercent.text = "%.0f".format(user.winPercentage*100f) + "%"

            // Bold the current user's name if they are on the leaderboard
            if (user.uid == FirebaseUtil.CurrentUser?.uid) {
                holder.userName.typeface = Typeface.DEFAULT_BOLD
            }
        }

        override fun getItemCount(): Int {
            return sortedData.size
        }

        inner class LeaderboardViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            var rank: TextView = itemView.rank
            var userName: TextView = itemView.user_name
            var money: TextView = itemView.money
            var wins: TextView = itemView.wins
            var losses: TextView = itemView.losses
            var winPercent: TextView = itemView.win_percentage
        }
    }
}
