package com.example.cse438.blackjack.util

import android.util.Log

class GameManager(cardList: ArrayList<Int>, cardMap: HashMap<Int, String>) {

    companion object {
        val BLACKJACK = 21
        val MIN_BET = 1
        val MAX_BET = 100
    }

    //private var player = currentPlayer
    private val deck = cardList
    private val cardMap = cardMap
    private var nextCardIndex = 0
    var playerHand = ArrayList<Int>()
    private var dealerHand = ArrayList<Int>()
    var holeCardId = -1
        private set
    var dealerUpCardId = -1
        private set
    var playerBet = -1
        private set
    private val numRegex = "\\d+".toRegex() // number finder regex

    init {
        if (cardList.size != 52) {
            Log.e("GAME_MANAGER", "Card list must be equal to 52. Found ${cardList.size}")
            // TODO throw error
        }
    }


    fun newGame(bet: Int) {
        // Clear the hands and reset variables
        playerHand.clear()
        dealerHand.clear()
        holeCardId = -1
        dealerUpCardId = -1
        nextCardIndex = 0

        // Set the bet
        playerBet = bet

        // Shuffle the deck for a new random card order
        deck.shuffle()

        // Deal player UP card
        playerHand.add(getCard())

        // Deal dealer DOWN card
        holeCardId = getCard()
        dealerHand.add(holeCardId)

        // Deal player UP card
        playerHand.add(getCard())

        // Deal dealer UP card
        dealerUpCardId = getCard()
        dealerHand.add(dealerUpCardId)
    }

    fun getCard(): Int {
        // TODO add index check?
        val cardId = deck[nextCardIndex]
        nextCardIndex++
        return  cardId
    }

    fun dealerHit(): Int {
        val cardId = getCard()
        dealerHand.add(cardId)
        return cardId
    }

    fun playerHit(): Int {
        val cardId = getCard()
        playerHand.add(cardId)
        return cardId
    }

    private fun getCardValue(cardId: Int): Int {
        var cardVal = -1

        // Get the card name from the card map
        val cardName = cardMap[cardId]

        // Look for a number in the name. A found number indicates a numerical card
        val num = numRegex.find(cardName!!)?.value

        if (num != null) {
            cardVal = num.toInt()
        }
        else if (cardName.contains("_ace")) {
            // Card is an ace
            cardVal = 11
        }
        else {
            // Card is a face card
            cardVal = 10
        }

        return cardVal
    }

    private fun getHandValue(hand: Iterable<Int>): Int {

        // Get the initial sum of the hand, assuming any aces are value 11
        var sum = hand.sumBy { card -> getCardValue((card)) }

        // Determine if there are any aces in this hand
        val temp = hand.filter { card -> cardMap[card]!!.contains("_ace") }
        var numAces = temp.size

        // Try to change aces to a value of 1 to come under 21
        if (sum > 21 && numAces > 0) {
            while (numAces > 0) {
                if (sum > 21) {
                    sum -= 10
                    numAces--
                } else {
                    break
                }
            }
        }

        return sum
    }

    val PlayerHandValue
        get() = getHandValue(playerHand)


    // Get the full dealer hand value, including the down card
    val DealerHandValue
        get() = getHandValue(dealerHand)

    // Get the dealer hand value of all cards except the first (down) one
    val DealerShownHandValue
        get() = getHandValue(dealerHand.subList(1, dealerHand.size))

    fun getPlayerCardsString(): String {
        return getCardsString(playerHand)
    }

    private fun getCardsString(hand: ArrayList<Int>): String {
        var str = ""
        for (cardId in hand) {
            str += cardMap[cardId] + ", "
        }
        return str
    }

    fun test() {
        val reversedMap = HashMap<String, Int>()
        for (kvp in cardMap) {
            val cardId = kvp.key
            val cardName = kvp.value
            reversedMap[cardName] = cardId
        }
        var hand = ArrayList<Int>()
        Log.d("GAME_MANAGER", "**** Begin Test ****")
        // Deal
        Log.d("GAME_MANAGER", "Dealing...")
        hand.add(reversedMap["clubs_queen"]!!)
        hand.add(reversedMap["diamonds2"]!!)
        var handValue = getHandValue(hand)
        Log.d("GAME_MANAGER", "Hand: ${getCardsString(hand)}")
        Log.d("GAME_MANAGER", "Hand value after deal: $handValue")

        // Hit - draw ace
        hand.add(reversedMap["clubs_ace"]!!)
        handValue = getHandValue(hand)
        Log.d("GAME_MANAGER", "Hand: ${getCardsString(hand)}")
        Log.d("GAME_MANAGER", "Hand value after hit: $handValue")

        // Hit - draw king
        hand.add(reversedMap["hearts_king"]!!)
        handValue = getHandValue(hand)
        Log.d("GAME_MANAGER", "Hand: ${getCardsString(hand)}")
        Log.d("GAME_MANAGER", "Hand value after hit: $handValue")

        Log.d("GAME_MANAGER", "**** End Test ****")


    }



}