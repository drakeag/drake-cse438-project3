In this file you should include:

Any information you think we should know about your submission
* Is there anything that doesn't work? Why?
* Is there anything that you did that you feel might be unclear? Explain it here.

A description of the creative portion of the assignment
* Describe your feature
* Why did you choose this feature?
* How did you implement it?

For my creative portion I chose to implement a betting feature. Before a hand
is dealt, a dialog pops up for the user to enter in an amount between $1 and 
$100. Once they enter in a valid bet the game begins. I chose this feauture
because betting is what typically makes games like Blackjack more interesting
and it better simulates a real game.

 The payout works as follows:
* Player loses - the amount bet is subtracted from the player's total money
* Player wins (non-blackjack) - the amount bet is added to the player's total money
* Player wins (blackjack) - the player receives 1.5x the amount bet
* Push - no money is lost or won
If a player's total money goes to 0 or negative the player is automatically awarded
$50 to continue playing. The total money a player has is displayed in the top left
corner. The leaderboard also displays the money a player has below their name. If
a player does not want to bet in order to view the leaderboard or sign out, they 
can cancel a bet which will leave a blank game on the screen. A new game can be
started by either navigating back to the Game screen from another screen or opening
the action bar and selecting New Game.

One other small feature I added was the ability to sign in using a Google account.
This (along with email sign in) was done using the Firebase built in FirebaseAuth
UI SDK. I wanted this feature because most people using Android have a Google
account and it's usually easier to use that account with its password than having
to maintain another password.

(10 points) Player can login and login data is stored in Firebase
(10 points) Player’s win/loss counts are displayed on startup
(10 points) The Player receives two cards face up, and the dealer receives one card face up and one card face down
(5 points) Swiping right allows the Player to hit
(5 points) Double tapping allows the Player to stand
(10 points) Cards being dealt are animated, and all cards are visible.
(5 points) If the Player goes over 21, they automatically bust
(10 points) The dealer behaves appropriately based on the rules described above
(10 points) Once the game is complete, the winner should be declared and the Firebase database should be updated appropriately
(5 points) A new round is automatically started after each round
(5 points) Player can logout
(10 points) A leaderboard is shown in a separate tab or activity and is consistent among installations of the app
(15 points) Creative portion!

This app works great and it looks great too. Fantastic job!

Total: 110 / 110